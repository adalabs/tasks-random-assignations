# Tasks Random Assignations

Randomly assign participants to tasks

## Build

```
$ make
```

## Usage

```
$ binaries/tasks-random_assignations-main <one-per-line-participants-file> <number-of-tasks>
```

## Example of individual assignations

Randomly assign 2 participants to 10 tasks.

```
$ cat /tmp/participants.txt
Love
Lace
$ make
$ binaries/tasks-random_assignations-main /tmp/participants.txt 10
Lace
Love
Lace
Lace
Lace
Love
Love
Lace
Love
Lace
```


## Example of group assignations

Randomly assign the 10 participants splitted in groups to 4 tasks

```
$ cat /tmp/participants.txt
Buzz Lightyear
Woody
Bo Peep
Billy
Goat
Gruff
Mr. Potato Head
Slinky Dog
Rex
Hamm
$ make
$ binaries/tasks-random_assignations-main /tmp/participants.txt 4 --group
Woody, Billy
Bo Peep, Mr. Potato Head, Hamm
Gruff, Slinky Dog
Buzz Lightyear, Goat, Rex
```


