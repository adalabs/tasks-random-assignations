with Ada.Command_Line,
     Ada.Text_IO;

package body Tasks is

   procedure Initialize
   is
      Handler : Ada.Text_IO.File_Type;
   begin
      Ada.Text_IO.Open (File => Handler,
                        Mode => Ada.Text_IO.In_File,
                        Name => Ada.Command_Line.Argument (1));
      while not Ada.Text_IO.End_Of_File (Handler) loop
         declare
            Participant : constant String := Ada.Text_IO.Get_Line (Handler);
         begin
            Tasks.Participants.Append (Participant);
         end;
      end loop;
      Ada.Text_IO.Close (File => Handler);

      Number_Of_Tasks := Natural'Value (Ada.Command_Line.Argument (2));

      if Ada.Command_Line.Argument_Count >= 3 then
         if Ada.Command_Line.Argument (3) = "--group" then
            Assignation_Mode := Group_Work;
         end if;
      end if;

      if Assignation_Mode in Group_Work and then Number_Of_Tasks > Natural (Participants.Length) then
         raise Not_Yet_Implemented with "[Limitation] group mode works only when number of tasks is less than number of participants";
      end if;

   end Initialize;

   procedure Log (Message : in     String)
   is
   begin
      Ada.Text_IO.Put_Line (Ada.Text_IO.Standard_Error,
                            Message);
   end Log;

end Tasks;
