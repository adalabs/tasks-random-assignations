with Ada.Command_Line,
     Ada.Exceptions,
     Ada.Numerics.Discrete_Random,
     Ada.Strings.Unbounded;

use Ada.Strings.Unbounded;

procedure Tasks.Random_Assignations.Main
is
   Proposed_Participant : Positive   := 1;
   Proposed_Group       : Positive   := 1;
begin
   Tasks.Initialize;

   case Assignation_Mode is
      when Individual_Work =>
         declare
            subtype Random_Number_Range is Natural range 1 .. Natural (Participants.Length);

            package Random_Numbers is new Ada.Numerics.Discrete_Random (Random_Number_Range);
            Generator : Random_Numbers.Generator;
         begin
            Random_Numbers.Reset (Generator);

            Max_Assignation := Natural (Float'Ceiling (Float (Number_Of_Tasks) / Float (Participants.Length)));

            for Index in 1 .. Number_Of_Tasks loop
               Tasks_Assigments.Append (0);
            end loop;

            for Index in 1 .. Natural (Participants.Length) loop
               Entity_Tasks_Number.Append (0);
            end loop;

            for Index in 1 .. Number_Of_Tasks loop
               declare
                  Try_Again : Boolean := True;
               begin
                  while Try_Again loop
                     Proposed_Participant := Random_Numbers.Random (Generator);
                     if Entity_Tasks_Number.Reference (Proposed_Participant) < Max_Assignation then
                        Tasks_Assigments.Reference (Index)                   := Proposed_Participant;
                        Entity_Tasks_Number.Reference (Proposed_Participant) := Entity_Tasks_Number.Reference (Proposed_Participant) + 1;
                        Try_Again                                            := False;
                     end if;
                  end loop;
               end;
            end loop;

            for Index in 1 .. Number_Of_Tasks loop
               Log (Participants.Element (Tasks_Assigments.Element (Index)));
            end loop;
         end;

      when Group_Work =>
         declare
            subtype Random_Number_Range is Natural range 1 .. Number_Of_Tasks;

            package Random_Numbers is new Ada.Numerics.Discrete_Random (Random_Number_Range);
            Generator : Random_Numbers.Generator;
         begin
            Random_Numbers.Reset (Generator);

            Max_Assignation := Natural (Participants.Length) / Number_Of_Tasks;

            for Index in 1 .. Number_Of_Tasks loop
               Groups.Append (Positive_Containers.Empty_Vector);
            end loop;

            for Index in 1 .. Natural (Participants.Length) loop
               declare
                  Try_Again : Boolean := True;
               begin
                  while Try_Again loop
                     Proposed_Group := Random_Numbers.Random (Generator);
                     if Natural (Groups.Reference (Proposed_Group).Element.Length) < Max_Assignation then
                        Groups.Reference (Proposed_Group).Append (Index);
                        Try_Again := False;
                     elsif Index > Number_Of_Tasks * Max_Assignation and then Natural (Groups.Reference (Proposed_Group).Element.Length) = Max_Assignation then
                        Groups.Reference (Proposed_Group).Append (Index);
                        Try_Again := False;
                     end if;
                  end loop;
               end;
            end loop;

            for Group of Groups loop
               declare
                  Group_Participants : Unbounded_String;
               begin
                  for Participant of Group loop
                     Append (Group_Participants, Participants.Element (Participant) & ", ");
                  end loop;
                  if Length (Group_Participants) >= 2 then
                     Delete (Group_Participants, Length (Group_Participants), Length (Group_Participants));
                     Delete (Group_Participants, Length (Group_Participants), Length (Group_Participants));
                  end if;
                  Log (To_String (Group_Participants));
               end;
            end loop;
         end;
   end case;


exception
   when E : Not_Yet_Implemented =>
      Log ("unexpected exception " & Ada.Exceptions.Exception_Message (E));
      Ada.Command_Line.Set_Exit_Status (200);
   when E : others =>
      Log ("unexpected exception " & Ada.Exceptions.Exception_Information (E));
      Ada.Command_Line.Set_Exit_Status (255);
end Tasks.Random_Assignations.Main;
