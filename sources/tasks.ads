with Ada.Containers.Indefinite_Vectors,
     Ada.Containers.Vectors;


package Tasks is
   Not_Yet_Implemented : exception;


   type Mode_Kinds is (Group_Work,
                       Individual_Work);

   procedure Initialize;

   procedure Log (Message : in     String);

   package Positive_Containers is new Ada.Containers.Indefinite_Vectors (Index_Type   => Positive,
                                                                         Element_Type => Positive);


   package Participant_Containers is new Ada.Containers.Indefinite_Vectors (Index_Type   => Positive,
                                                                            Element_Type => String);

   use type Positive_Containers.Vector;
   package Group_Containers is new Ada.Containers.Indefinite_Vectors (Index_Type   => Positive,
                                                                      Element_Type => Positive_Containers.Vector);

   package Natural_Containers is new Ada.Containers.Vectors (Index_Type   => Positive,
                                                             Element_Type => Natural);

   Participants        : Participant_Containers.Vector;
   Entity_Tasks_Number : Natural_Containers.Vector;
   Tasks_Assigments    : Natural_Containers.Vector;
   Groups              : Group_Containers.Vector;
   Number_Of_Tasks     : Natural := 0;
   Max_Assignation     : Natural := 0;
   Assignation_Mode    : Mode_Kinds := Individual_Work;
end Tasks;
