
build: clean
	make build-only

build-only:
	gprbuild -P tasks-random_assignations.gpr

clean:
	rm -rf objects/* binaries/* sources/.clang-format
